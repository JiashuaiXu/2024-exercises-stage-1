#!/usr/bin/env bash

EXECUTABLE=$1

KO=$(readelf -s ${EXECUTABLE} | grep FILE | sed '1d' | awk '{print $8}')

MODULE_MESSAGE_1=$(dmesg | grep "Delay 2s!")

insmod $EXECUTABLE

sleep 3

MODULE_MESSAGE_2=$(dmesg | grep "Delay 2s!")

rmmod ${KO%.c}

if [ -n "$MODULE_MESSAGE_2" ] && [ -z "$MODULE_MESSAGE_1" ]; then
  echo "Test passed."
  exit 0
else
  echo "Test failed. Expected have '$MODULE_MESSAGE_2' but got empty"
  exit 1
fi
